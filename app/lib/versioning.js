/* eslint-disable no-unused-vars */



const restify = require('restify');
const errors = require('restify-errors');
const semver = require('semver');

module.exports = function versioning(versionOptions) {
  let options = versionOptions;
  options = options || {};

  options.prefix = options.prefix || '';

  return function theRequest(req, res, next) {
    req.originalUrl = req.url;
    req.url = req.url.replace(options.prefix, '');

    const pieces = req.url.replace(/^\/+/, '').split('/');
    let version = pieces[0];

    version = version.replace(/v(\d{1})\.(\d{1})\.(\d{1})/, '$1.$2.$3');
    version = version.replace(/v(\d{1})\.(\d{1})/, '$1.$2.0');
    version = version.replace(/v(\d{1})/, '$1.0.0');

    if (semver.valid(version)) {
      req.url = req.url.replace(pieces[0], '');
      req.headers = req.headers || [];
      req.headers['accept-version'] = version;
    } else {
      return next(new errors.InvalidVersionError({
        statusCode: 409,
        message: 'Version not supported',
      }));
    }

    return next();
  };
};
