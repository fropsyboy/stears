/* eslint-disable no-template-curly-in-string */
/**
 * Created by Adeyinka Micheal
 */
require('dotenv').config();

const appName = 'Stears Interview';

const config = {
  app_name: appName,
  delimiter: ':\n',
  api_server: {
    port: process.env.API_PORT,
  },
  rabbitMQ: {
    host: process.env.RABBIT_HOST,
    port: process.env.RABBIT_PORT,
    user: process.env.RABBIT_USER,
    pass: process.env.RABBIT_PASS,
    queue: process.env.RABBIT_QUEUE,
    vhost: process.env.VHOST
  },

  nodeMailer:{
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD,
    service: process.env.MAIL_SERVICE
  },
  MySql: {
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    pass: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DATABASE,
  },
};

module.exports = config;
