
/**
 * 
 * objective: building to scale
 */
const rabbit = require('amqplib');
const Sequelize = require('sequelize');
const winston = require("winston");
const morgan = require("morgan");


const config = require("../config/config");
const serviceLocator = require("../lib/serviceLocator.js");
//controller(s)

const dashboardController = require("../controllers/dashboard");

const dashboardService = require("../services/dashboard");

serviceLocator.register("logger", () => {
	const consoleTransport = new (winston.transports.Console)({
		datePattern: "yyyy-MM-dd.",
		prepend: true,
		json: false,
		colorize: true,
		level: process.env.ENV === "development" ? "debug" : "info",
	});
	const transports = [consoleTransport];
	const winstonLogger = winston.createLogger({
		transports: transports
	});
	return winstonLogger;
});

serviceLocator.register("requestlogger", () => morgan("common"));
/**
 * Returns a RabbitMQ connection instance.
 */
serviceLocator.register('database', (servicelocator) => {

	const connectionString = new Sequelize(config.MySql.database, config.MySql.user, config.MySql.pass, {
		dialect: 'mysql',
		host: config.MySql.host,
		port: config.MySql.port
	  });

	  console.log(`MySQL Connected Successfully ${config.MySql.host}`);
  
	  return connectionString;
  });


/**
 * Returns a RabbitMQ connection instance.
 */
serviceLocator.register('rabbitmq', (servicelocator) => {
	const connectionString = `amqp://${config.rabbitMQ.user}:${config.rabbitMQ.pass}@${config.rabbitMQ.host}/${config.rabbitMQ.vhost}`;
  
	return rabbit.connect(connectionString, (err, connection) => new Promise((resolve, reject) => {
	  // If the connection throws an error
	  if (err) {
		console.log(`RabbitMQ connection error: ${err}`)
		return reject(err);
	  }
  
	  connection.on('error', (connectionError) => {
		console.log(`RabbitMQ connection error: ${connectionError}`)
		process.exit(1);
	  });
  
	  connection.on('blocked', (reason) => {
		console.log(`RabbitMQ connection blocked: ${reason}`);
		process.exit(1);
	  });
  
	  // If the Node process ends, close the RabbitMQ connection
	  process.on('SIGINT', () => {
		connection.close();
		process.exit(0);
	  });
  
	  return resolve(connection);
	}));
  });

/**
 * Creates an instance of the Responses service
 */
serviceLocator.register("dashboardService", (servicelocator) => {
	const databaseClient = servicelocator.get("database");
	return new dashboardService(databaseClient);
});


serviceLocator.register("dashboardController", (servicelocator) => {
   const dashboardService = servicelocator.get("dashboardService");

   return new dashboardController(dashboardService);
});

module.exports = serviceLocator;
